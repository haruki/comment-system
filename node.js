// -------------------- node ----------------------

function node(ctx)
{
  return ["node", ctx, []];
}

function isNode(x)
{
  return x[0] == "node";
}

function ctsOf(x)
{
  if (isNode(x))
  {
    return x[1];
  }
  else
  {
    throw(x, "is not node!");
  }
}


function childsOf(x)
{
  if (isNode(x))
  {
    return x[2];
  }
  else
  {
    throw x, "is not node!";
  }
}


function push(child, n)
{
  let childs = childsOf(n);
  childs.push(child);
  return n;
}


// 缩进需要控制
function nodeToStr(tree)
{
  show(tree)
  let childs = childsOf(tree); 
  let str = String(ctsOf(tree));
  if (childs.length == 0)
  {
    return str;
  }
  else
  {
    return childs.reduce((accum, node) => accum
                                            + " "
                                            + nodeToStr(node),  str + "\n");
  }
}

// --------------- test ----------------

var show = console.log;

var root = node(null)

//        null
//         1
//     1.1   2
//       2.1   3

push(push(push(node(3), push(node(2.1), node(2))), push(node(1.1), node(1))), root)

var t1 = push(push(node(3), push(node(2.1), node(2))), push(node(1.1), node(1)))

show(nodeToStr(t1))

show(nodeToStr(root))
